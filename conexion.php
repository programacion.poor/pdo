<?php

$host       = "localhost";
$usuario   = "root";
$contraseña   = "admin";
$base_datos     = "database";
$dsn        = "mysql:host=$host;dbname=$base_datos";
// $opciones    = array(
//                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
//              );
try{
    $conexion = new PDO($dsn, $usuario, $contraseña);
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "se ha establecido una conexion con el servidor de base de datos.";
}
catch(PDOException $e){
    echo "Error en a conexion a la base de datos: ".$e->getMessage();
}
$conexion = null;